# Projet Tweetoscope 2021 - Equipe 8

Bienvenue sur la page gitlab du projet tweetoscope de l'équipe 15 composée d'Adrien Antonov, Alexandre Daly et Rémi Schlosser!

# Lien vers notre vidéo explicative :
https://we.tl/t-5BZMgRFp79

# Règles de commit
Se référer à ce [guide](https://gitmoji.dev/) pour les bonnes pratiques de commit!

# TODO  
### Tweetgenerator  
- [x] Implémenté  
- [x] Conteneurisé  
- [X] Commentaires  
- [X] CI
- [ ] CD 
- [ ] Tests

### Tweetcollector  
- [x] Implémenté  
- [x] Conteneurisé  
- [X] Commentaires  
- [X] CI
- [ ] CD
- [ ] Tests

### Hawkes
- [X] Implémenté
- [X] Conteneurisé
- [X] Commentaires
- [X] CI
- [ ] CD
- [ ] Tests

### Randomforest
- [ ] Implémenté  
- [ ] Conteneurisé  
- [ ] Commentaires  
- [ ] CI
- [ ] CD
- [ ] Tests

### Logger
- [X] Implémenté  
- [X] Conteneurisé  
- [X] Commentaires  
- [X] CI
- [ ] CD
- [ ] Tests

# Installer le projet sur intercell

```
# Cloner le repo
git clone git@gitlab-student.centralesupelec.fr:2018schlosser/tweetoscope_2021_15.git
cd tweetoscope_2021_15

kubectl apply -f zookeeper-and-kafka.yml -n cpusdi1-3-ns
kubectl apply -f tweetoscope-deploy.yml -n cpusdi1-3-ns
# Attendre un peu, puis lire les logs du logger
# Le nom du pod sera probablement à modifier
kubectl logs logger-deployment-5f85c7799d-74lh4 -n cpusdi1-3-ns -f
```

Les informations s'affichent dans les logs du pod du logger.
