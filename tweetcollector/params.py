import os.path
import json
import configparser

class Collector:
    kafka_brokers: str
    input_topic: str
    output_series_topic: str
    output_properties_topic: str
    observations: list
    terminated: int
    min_cascade_size: int

    def __init__(self, config_filename: str) -> None:
        if not os.path.isfile(config_filename):
            print("Cannot open", config_filename, " for reading parameters.")
        
        config = configparser.ConfigParser()
        config.read(config_filename)

        self.kafka_brokers = config['kafka']['brokers']
        self.input_topic = config['topic']['input']
        self.output_series_topic = config['topic']['output_series']
        self.output_properties_topic = config['topic']['output_properties']
        self.observations = json.loads(config['times']['observations'])
        self.terminated = config['times']['terminated']
        self.min_cascade_size = config['cascade']['min_cascade_size']
    
    def __str__(self) -> str:
        """
        Class Collctor's print
        """

        collector = "[kafka]\n" + \
                    "    brokers=" + self.kafka_brokers + "\n" + \
                    "[topic]\n" + \
                    "    input=" + self.input_topic + "\n" + \
                    "    output_series=" + self.output_series_topic + "\n" + \
                    "    output_properties=" + self.output_properties_topic + "\n" + \
                    "[times]\n" + \
                    "    observations=" + str(self.observations) + "\n" + \
                    "    terminated=" + str(self.terminated) + "\n" + \
                    "[cascade]\n" + \
                    "    brokers=" + str(self.min_cascade_size)
        return collector
    