import json
from source import Tweet

class Cascade:
    cid: str
    msg: str = ""
    time_first_tweet: int
    time_last_tweet: int
    times_magnitudes: dict
    source: int

    def __init__(self, tweet: Tweet, observations) -> None:
        self.cid = tweet.info[8::] # "cascade=1234 --> 1234"
        self.msg = tweet.msg
        self.time_first_tweet = tweet.time
        self.time_last_tweet = tweet.time
        self.times_magnitudes = {}
        self.times_magnitudes[tweet.time] = tweet.magnitude
        self.source = tweet.source
    
    def cascade_update(self, tweet) -> None:
        self.times_magnitudes[tweet.time] = tweet.magnitude
        self.time_last_tweet = tweet.time

class Processor:
    source: int
    source_time: int
    priority_queue: dict = {}
    fifo: dict = {}
    symbole_table: dict = {}

    def __init__(self, tweet: Tweet) -> None:
        self.source = tweet.source
        self.source_time = tweet.time

    def send_partial_cascade(self, observations: list) -> list:
        to_send = []
        for T_obs in observations:
            cid_to_send = []
            if len(self.fifo[T_obs]) > 0:
                cascade = self.fifo[T_obs][0]
                while(self.source_time - cascade.time_first_tweet > T_obs):
                    tm_to_send = {}
                    for time in cascade.times_magnitudes:
                        if (time - cascade.time_first_tweet <= T_obs):
                            tm_to_send[time] = cascade.times_magnitudes[time]

                    msg_series = "{"  + \
                                "\"type\" : \"serie\"" + \
                                ", \"cid\" : \"" + cascade.cid + "\"" +\
                                ", \"msg\": \"" + cascade.msg + '"' + \
                                ", \"T_obs\" : " + str(T_obs) + \
                                ",\"tweets\" :" + json.dumps(tm_to_send) + \
                                "}"

                    if cascade.cid in cid_to_send:
                        print("Duplicated key :", cascade.cid, ", T_obs :", T_obs)
                    else:
                        to_send.append(msg_series)
                        cid_to_send.append(cascade.cid)
                    self.fifo[T_obs].pop(0)

                    if len(self.fifo[T_obs]) > 0:
                        cascade = self.fifo[T_obs][0]
                    else:
                        break
        return to_send
    
    def get_oldest_cascade_updated(self):
        oldest_id = "no_id"
        if len(self.priority_queue) > 0:
            for cid in self.priority_queue:
                if oldest_id == "no_id":
                    oldest_id = cid
                else:
                    if self.priority_queue[cid].time_last_tweet < self.priority_queue[oldest_id].time_last_tweet:
                        oldest_id = cid
        return oldest_id


    def send_terminated_cascade(self, t_over:int, minimum_size: int) -> list:
        to_send = []
        oldest_id = self.get_oldest_cascade_updated()
        if oldest_id != "no_id":
            cascade = self.priority_queue[oldest_id]
            while(self.source_time - cascade.time_last_tweet > t_over and oldest_id != "no_id"):
                if len(cascade.times_magnitudes) >= minimum_size:
                    msg_properties = "{"  + \
                                     "\"type\" : \"size\"" + \
                                     ", \"cid\" : " + cascade.cid + \
                                     ", \"n_tot\" : " + str(len(cascade.times_magnitudes)) + \
                                     ", \"t_end\" : " + str(cascade.time_last_tweet) + \
                                     "}"

                    to_send.append(msg_properties)
                    self.priority_queue.pop(oldest_id)
                    oldest_id = self.get_oldest_cascade_updated()
                    if oldest_id != "no_id":
                        cascade = self.priority_queue[oldest_id]
                    else:
                        break
                else:
                    self.priority_queue.pop(oldest_id)
                    oldest_id = self.get_oldest_cascade_updated()
                    if oldest_id != "no_id":
                        cascade = self.priority_queue[oldest_id]
                    else:
                        break
        return to_send

