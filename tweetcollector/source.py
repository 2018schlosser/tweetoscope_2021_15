class Tweet:
    type: str
    msg: str
    time: int
    magnitude: int
    source: int
    info: str

    def __init__(self, msg) -> None:
        self.type = msg.value['type']
        self.msg = msg.value['msg']
        self.time = msg.value['t']
        self.magnitude = msg.value['m']
        self.source = msg.key
        self.info = msg.value['info']