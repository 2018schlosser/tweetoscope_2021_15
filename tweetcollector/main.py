import os
import argparse
import json
from kafka import KafkaConsumer
from kafka import KafkaProducer

from params import Collector
from cascade import Cascade, Processor
from source import Tweet
import logger

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--config-file', type=str, required=True, help="the config file")
args = parser.parse_args()

params = Collector(args.config_file)
print("Parameters : ")
print("----------")
print(params)

# Create consumer
consumer = KafkaConsumer(params.input_topic,
  bootstrap_servers = params.kafka_brokers,
  value_deserializer=lambda v: json.loads(v.decode('utf-8')),
  key_deserializer= lambda v: v.decode()
)

# Create producer
producer = KafkaProducer(
  bootstrap_servers = params.kafka_brokers,
  value_serializer=lambda v: json.dumps(v).encode('utf-8'),
  key_serializer=str.encode
)

# Create logger
logger = logger.get_logger('Cascade', broker_list=params.kafka_brokers)

processors = {}
key_handle = {}
observations = params.observations
t_over = params.terminated

for msg in consumer:
    tweet = Tweet(msg)
    cascade_id = tweet.info[8::]

    if not(tweet.source in processors):
        processor = Processor(tweet)
        processors[tweet.source] = processor
        for t_obs in observations:
            processor.fifo[t_obs] = []
    
    procesor = processors[tweet.source]

    if tweet.type == "tweet":
        cascade = Cascade(tweet, observations)
        processor.time_source = tweet.time
        processor.priority_queue[cascade_id] = cascade

        for t_obs in observations:
            processor.fifo[t_obs].append(cascade)

        processor.symbole_table[cascade_id] = cascade
    
    else:
        if cascade_id in processor.symbole_table:
            cascade = processor.symbole_table[cascade_id]
            cascade.cascade_update(tweet)
            processor.source_time = tweet.time

    # Partial cascades
    series_to_send = processor.send_partial_cascade(observations)
    for msg_series in series_to_send:
        msg = json.loads(msg_series)
        producer.send(params.output_series_topic, value=msg, key = msg['cid'])

    producer.flush()

    # Cascade properties
    to_send = processor.send_terminated_cascade(int(t_over), int(params.min_cascade_size))
    for msg_properties in to_send:
        msg = json.loads(msg_properties)
        i = 0
        for t_obs in observations:
            producer.send(params.output_properties_topic, value=msg, key=str(t_obs), partition=i)
        logger.info('[t_end={}][Cascade n°{}] {} tweets observés.'.format(msg['t_end'], msg['cid'], msg['n_tot']))
    producer.flush()
