import argparse
import json
from kafka import KafkaConsumer, KafkaProducer
import numpy as np

from utils import prediction, compute_MAP
import logger

# Arguments
# python hawkes.py --broker-list localhost:9092
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--broker-list', type=str, required=True, help="the broker list")
parser.add_argument('--input-topic', type=str, required=True, help="the input topic")
parser.add_argument('--output-topic', type=str, required=True, help="the output topic")
args = parser.parse_args()

## Kafka
# Consumer
consumer = KafkaConsumer(args.input_topic,
                          bootstrap_servers = args.broker_list,
                          value_deserializer =lambda v: json.loads(v.decode('utf-8')),
                          auto_offset_reset="earliest",
                          group_id = "CascadeSeriesConsumerGroup")

# Producer
producer = KafkaProducer(
  bootstrap_servers = args.broker_list,
  value_serializer=lambda v: json.dumps(v).encode('utf-8'),
  key_serializer=lambda v : json.dumps(v).encode('utf-8'))

# Logger
logger = logger.get_logger('Hawkes', broker_list=args.broker_list)

# Estimation

alpha, mu = 2.4, 10 # La distribution des magnitudes suit une loi à puissance négative 
                    # de pararamètres alpha, mu

for msg in consumer:
    msg = msg.value

    T_obs = msg['T_obs']
    times_magnitudes = list(msg['tweets'].items())
    times_magnitudes = np.asarray(times_magnitudes)
    times_magnitudes = times_magnitudes.astype(np.float64).astype(np.int64)
    n_obs = len(times_magnitudes)
    cid = msg['cid']
    message = msg['msg']

    # Démarrer la cascade à t=0
    times_magnitudes[:,0] -= times_magnitudes[0][0]

    # Estimation
    tmp1, tmp2 = compute_MAP(times_magnitudes, T_obs, alpha, mu)
    p = tmp2[0]
    beta = tmp2[1]
    params = [p, beta]

    # Prediction
    n_total = prediction(params, times_magnitudes, alpha, mu, T_obs)
    n_supp = round(n_total - n_obs)

    msg_to_send = {
        'type': 'parameters',
        'cid': cid,
        'msg': message, 
        'n_obs': n_obs,
        'n_supp': n_supp,
        'params': params
    }
    producer.send(args.output_topic, value=msg_to_send, key=T_obs)
    logger.info('[T_obs={}][Cascade n°{}] {} tweets observés, estimation de {} tweets au total.'.format(T_obs, cid, n_obs, n_obs+n_supp))
producer.flush()
